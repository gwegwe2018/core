<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Core\Form,
    Project\Core\Form\Config;

class PortalFormSupport extends CBitrixComponent {

    protected function getFormId() {
        throw new Exception('Задайте форму');
    }

    protected function filterForm(&$arData) {
        throw new Exception('Задайте обработчик формы');

        $request = Application::getInstance()->getContext()->getRequest();
        if (empty($request->getPost('MESSAGE'))) {
            $this->arResult['ERROR']['MESSAGE'] = 'Поле «Вопрос» не заполнено';
        } else {
            $arData['MESSAGE'] = $request->getPost('MESSAGE');
        }
        if ($this->arResult['IS_USER']) {
            $arData['EMAIL'] = cUser::GetEmail();
        } elseif (empty($request->getPost('EMAIL'))) {
            $this->arResult['ERROR']['EMAIL'] = 'Поле «E-mail» не заполнено';
        } else {
            $arData['EMAIL'] = $request->getPost('EMAIL');
        }
    }

    protected function sendForm($arData) {
        return Form::add($this->getFormId(), $arData);
    }

    public function executeComponent() {
        if (Loader::includeModule('project.core')) {
            $request = Application::getInstance()->getContext()->getRequest();
            $this->arResult['IS_USER'] = cUser::IsAuthorized();
            $this->arResult['FORM_SEND'] = $this->GetTemplateName();
            $this->arResult['IS_AJAX'] = ($request->isPost() and check_bitrix_sessid() and $request->getPost('form-send') === $this->arResult['FORM_SEND']);
            $this->arResult['ERROR'] = array();
            if ($this->arResult['IS_AJAX']) {
                $arData = array();
                $this->filterForm($arData);
                if (empty($this->arResult['ERROR'])) {
                    if ($this->sendForm($arData)) {
                        $this->arResult['IS_SEND'] = true;
                        return $this->includeComponentTemplate();
                    }
                    global $strError;
                    $this->arResult['ERROR']['FORM'] = $strError ?: 'Возникли проблемы на сервере, попробуйте повторить позднее.';
                }
                $this->arResult['DATA'] = $arData;
            }
            $this->includeComponentTemplate();
        }
    }

}
