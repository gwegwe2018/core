<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset;

class ProjectAjaxWrapper extends CBitrixComponent {

    private function startWrapperTemplate() {
        if (empty($this->arResult['IS_FULL'])) {
            return;
        }
        Asset::getInstance()->addJs($this->arResult["SCRIPT"]);
        $jsParams = array(
            "AJAX" => $this->arResult["AJAX"],
            "TEMPLATE_NAME" => $this->arResult["TEMPLATE_NAME"],
            "PARAM" => $this->arResult["PARAM"],
            "CONTANER" => '.' . $this->arResult["CONTANER"],
            "CONTANER_FORM" => '.' . $this->arResult["CONTANER_FORM"],
            "CONTANER_FORM_BUTTON" => '.' . $this->arResult["CONTANER_FORM"],
            "CONTANER_LIST" => '.' . $this->arResult["CONTANER_LIST"],
            "CONTANER_MORE" => '.' . $this->arResult["CONTANER_MORE"],
        );
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> = new jsProjectAjaxWrapper(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
        $this->arResult['IS_FULL'] = $this->arParams['IS_UPDATE'] ?: empty($this->arResult['IS_AJAX']);
        $this->arResult['PAGEN'] = ceil($this->arParams['PAGEN'] ?: 1);
        $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
        $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
        $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
        $this->arResult["PARAM"] = $this->arParams["PARAM"] ?: '';
        $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult["PARAM"] ?: '');

        $this->arResult['CONTANER'] = 'ajax-wrappe-contaner' . $key;
        $this->arResult['CONTANER_FORM'] = 'ajax-wrapper-form' . $key;
        $this->arResult['CONTANER_FORM_BUTTON'] = 'ajax-wrapper-button' . $key;
        $this->arResult['CONTANER_LIST'] = 'ajax-wrapper-list' . $key;
        $this->arResult['CONTANER_MORE'] = 'ajax-wrapper-more' . $key;
        $this->arResult['JS_OBJECT'] = 'jsAjaxWrapper' . $key;
        if (empty($this->arResult['IS_AJAX'])) {
            Asset::getInstance()->addJs($this->arResult["SCRIPT"]);
            $this->startWrapperTemplate();
        }
        $this->includeComponentTemplate();
    }

}
