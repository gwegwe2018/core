(function (window) {
    'use strict';

    if (window.jsProjectAjaxWrapper) {
        return;
    }

    let loader = function (el) {
        this.el = $(el);
        this.el.data('html', this.el.html()).text("Загрузка... ");
    };

    loader.prototype.el = function () {
        return this.el;
    };

    loader.prototype.success = function (isActive) {
        if (isActive) {
            this.el.html(this.el.data('html')).html(this.el.data('html'));
        } else {
            this.el.removeClass('active').html(this.el.data('html')).html(this.el.data('html'));
        }
    };

    loader.prototype.fail = function () {
        this.el.text("Не удалось загрузить страницу, попробуйте позже")
                .delay(1500)
                .queue(function (n) {
                    $(this).html(this.el.data('html'));
                    n();
                });
    };


    window.jsProjectAjaxWrapper = function (arParams) {
        let self = this;
        self.config = {
            ajax: arParams.AJAX,
            contaner: arParams.CONTANER,
            contanerForm: arParams.CONTANER_FORM,
            contanerFormButton: arParams.CONTANER_FORM_BUTTON,
            contanerList: arParams.CONTANER_LIST,
            contanerMore: arParams.CONTANER_MORE,
            start: false,
            stop: false
        };
        self.param = {
            TEMPLATE_NAME: arParams.TEMPLATE_NAME,
            PAGEN_1: 2,
            IS_UPDATE: 0,
            PARAM: arParams.PARAM
        };

        $(document).on('submit', self.config.contanerForm, function () {
            console.log(this, $(this).serialize());
            self.sendPost(self.param.contanerFormButton, $(this).serialize());
            return false;
        });

    };

    window.jsProjectAjaxWrapper.prototype.sendPost = function (el, post) {
        let self = this;
        let load = new loader(el);
        $.post(self.config.ajax +'?'+ $.param(self.param), post, function (data) {
            if (data) {
                if (self.start) {
                    self.start(load.el());
                }
                $(self.config.contaner).html(data.content);
                console.log($(self.config.contaner), data.content);
                load.success(self.stop);
                if (self.stop) {
                    self.stop(load.el());
                }
            } else {
                load.fail();
            }
        }, 'json').fail(function () {
            load.fail();
        });
    }


    window.jsProjectAjaxWrapper.prototype.reload = function (el) {
        this.param.PAGEN_1 = 1;
        this.param.IS_UPDATE = 1;
        this.loadAjax(el);
    };

    window.jsProjectAjaxWrapper.prototype.loadAjax = function (el) {
        let self = this;
        let load = new loader(el);
        $.get(self.config.ajax, self.param, function (data) {
            if (data) {
                self.param.PAGEN_1++;
                if (self.start) {
                    self.start(load.el());
                }
                if (self.param.IS_UPDATE) {
                    $(self.config.contaner).html(data.content);
                } else {
                    $(self.config.contanerList).append(data.content);
                }
                load.success(self.stop);
                if (self.stop) {
                    self.stop(load.el());
                }
            } else {
                load.fail();
            }
            self.param.IS_UPDATE = 0;
        }, 'json').fail(function () {
            load.fail();
        });
    };

   window.jsProjectAjaxWrapper.prototype.showNext = function (is, hidden) {
        if(!hidden) {
            hidden = 'hidden';
        }
        if (is) {
            $(this.config.contanerMore).parent().removeClass(hidden);
        } else {
            $(this.config.contanerMore).parent().addClass(hidden);
        }
    };


})(window);