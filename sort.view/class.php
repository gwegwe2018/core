<?php

class ProjectSortView extends CBitrixComponent {

    public function executeComponent() {

        $arParamsView = array(
            'card' => array(),
            'line' => array()
        );
        $arParamsSort = array(
            'price-desc' => array(
                'sort' => array(
                    'catalog_PRICE_1' => 'DESC'
                ),
                'name' => 'цене: Дорогие — Дешевые'
            ),
            'price-asc' => array(
                'sort' => array(
                    'catalog_PRICE_1' => 'ASC'
                ),
                'name' => 'цене: Дешевые — Дорогие',
            ),
//            'img-on' => 'изображению: есть — нет',
//            'img-off' => 'изображению: нет — есть',
            'name-asc' => array(
                'sort' => array(
                    'NAME' => 'ASC'
                ),
                'name' => 'названию: А — я'
            ),
            'name-desc' => array(
                'sort' => array(
                    'NAME' => 'DESC'
                ),
                'name' => 'названию: я — А'
            ),
            'availability-desc' => array(
                'sort' => array(
                    'CATALOG_QUANTITY' => 'DESC'
                ),
                'name' => 'наличию: много — мало'
            ),
            'availability-asc' => array(
                'sort' => array(
                    'CATALOG_QUANTITY' => 'ASC,NULLS'
                ),
                'name' => 'наличию: мало — много'
            ),
            'date-desc' => array(
                'sort' => array(
                    'CREATED' => 'ASC'
                ),
                'name' => 'дате: новые — старые'
            ),
            'date-asc' => array(
                'sort' => array(
                    'CREATED' => 'ASC'
                ),
                'name' => 'дате: старые — новые'
            ),
            'rating-asc' => array(
                'sort' => array(
                    'PROPERTY_RATING' => 'ASC,NULLS'
                ),
                'name' => 'рейтингу: Хорошие — Плохие'
            ),
            'rating-desc' => array(
                'sort' => array(
                    'PROPERTY_RATING' => 'DESC'
                ),
                'name' => 'рейтингу: Плохие — Хорошие'
            ),
            'comment-desc' => array(
                'sort' => array(
                    'PROPERTY_FORUM_MESSAGE_CNT' => 'DESC'
                ),
                'name' => 'количеству отзывов: Много — Мало'
            ),
            'comment-asc' => array(
                'sort' => array(
                    'PROPERTY_FORUM_MESSAGE_CNT' => 'ASC'
                ),
                'name' => 'количеству отзывов: Мало — Много'
            )
        );
        if (isset($this->arParams['SORT'])) {
            $arParamsSort = array_merge(array(
                'search' => array(
                    'sort' => array(
                        'PROPERTY_FORUM_MESSAGE_CNT' => 'ASC'
                    ),
                    'name' => 'Релевантности поисковому запросу'
                )
                    ), $arParamsSort);
        }
        list($this->arResult['itemView'], $this->arResult['arView']) = Project\Core\Sort::init('view', $arParamsView);
        list($this->arResult['itemSort'], $this->arResult['arSort']) = Project\Core\Sort::init('sort', $arParamsSort);
        $this->includeComponentTemplate();
        return array($this->arResult['itemView'], $this->arResult['itemSort']);
    }

}
