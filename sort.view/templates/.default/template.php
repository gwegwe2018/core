<div class="toolbar">
    <form action="" class="form-inline OrderFilterForm">
        <div class="views">
            <label>Вид списка:</label>
            <a href="<?= $arResult['arView']['card']['url'] ?>" class="<?= isset($arResult['arView']['card']['select']) ? 'grid-active' : 'grid' ?>"></a>
            <a href="<?= $arResult['arView']['line']['url'] ?>" class="<?= isset($arResult['arView']['line']['select']) ? 'list-active' : 'list' ?>"></a>
        </div>
        <div class="sort">
            <div class="sort-by">
                <label>Сортировать по:</label>
                <span class="select-box">
                    <select name="sort" onchange="window.location.href = $(this).val();">
                        <? foreach ($arResult['arSort'] as $key => $value) { ?>
                            <option value="<?= $value['url'] ?>" <? if (isset($value['select'])) { ?>selected="selected"<? } ?> ><?= $value['name'] ?></option>
                        <? } ?>
                    </select>
                </span>
            </div>
        </div>
    </form>
    <div class="clear"></div>
</div>
<form method="get">
    <div class="filters">
        <div class="clear"></div>
    </div>
</form>